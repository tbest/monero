#!/usr/bin/perl
use strict;
use warnings;

my $repetitions= shift;

my $loopruntime=60*105;

#run xmr-stak for the given time in seconds
sub RunXMRStak{
    my $runtime=shift;
    
    #run xmr-stak in parallel
    system("./xmr-stak &");

    #wait for some time
    sleep ($runtime);

    #and stop xmr-stak
    system("pkill xmr-stak");
}

chdir("~/monero/bin" );

my $loopcounter=$repetitions;

do
{
    #now run xmr-stak with the optimum setting 
    RunXMRStak($loopruntime);
    $loopcounter--;
}
while($loopcounter!=0);

















