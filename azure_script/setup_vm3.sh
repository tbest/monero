#!/usr/bin/env bash
sudo apt-get -y update       
sudo apt-get -y upgrade  
sudo apt -y install libmicrohttpd-dev libssl-dev cmake build-essential libhwloc-dev leafpad git xauth
git clone https://tbest@bitbucket.org/tbest/monero.git

cd monero
chmod -R 777 *

cmake . -DCUDA_ENABLE=OFF -DOpenCL_ENABLE=OFF
make

#cd /home/theboss/
#cd ~
#cd monero
cd ..
pwd > path.txt

cp ./monero/config/config.txt ./monero/bin/config.txt
#cp config/cpu.txt bin/cpu.txt
cp ./monero/config/pools.txt ./monero/bin/pools.txt

cd ./monero/bin/

sudo sysctl -w vm.nr_hugepages=128

#./xmr-stak
